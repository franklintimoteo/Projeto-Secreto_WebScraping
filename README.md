## Entenda como funciona:

Bibliotecas necessárias:
* [urllib](https://docs.python.org/3.5/library/urllib.html)
* [re](https://docs.python.org/3/library/re.html)
* [time](https://docs.python.org/3/library/time.html)
* [requests](http://docs.python-requests.org/en/master/)
* [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/)

Obs: Nem todas são utilizadas em todos os scripts.

### Search vida de programador

Para utilizar essa função você deve chama-la junto com o url da página principal do website Vida de Programador (http://www.vidadeprogramador.com.br) veja um exemplo:


```python
from searchVidaProgramador import getTirinha

url = "http://www.vidadeprogramador.com.br"
conteudo = getTirinha(url)
```
O retorno será uma url da imagem, encontrada na primeira postagem, com a data atual, algo como:

```
http://vidadeprogramador.com.br/uploads/2016/09/tirinha1613.png
```

Caso queria encontrar mais de uma imagem, habilite o limite para um valor de 1-10. Onde 10 é a quantidade total de tirinhas dispostas na página principal.

```python
url = "http://www.vidadeprogramador.com.br"
conteudo = getTirinha(url, limit=3)

#Resposta:
#{'http://vidadeprogramador.com.br/uploads/2016/09/tirinha1.png',
#'http://vidadeprogramador.com.br/uploads/2016/09/tirinha2.png',
#'http://vidadeprogramador.com.br/uploads/2016/09/tirinha3.png'}
```

Observações: 
* Caso a data da postagem seja diferente da atual, será retornado uma
lista vazia.
* Caso não ocorra algum erro durante a busca, será retornado um None.

### Search Packtpub
### Fluxograma

<img src="https://gitlab.com/franklintimoteo/Projeto-Secreto_WebScraping/raw/7b29c5ca0965bd7515aa5e30e249476b0e93007a/Fluxogramaok.png" alt="">

